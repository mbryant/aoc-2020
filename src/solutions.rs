use std::collections::{HashSet, HashMap};
use std::cmp;
use std::iter::Iterator;
use std::hash::BuildHasher;
use itertools::Itertools;
use regex::Regex;
use parse_display::FromStr;
use bit_set::BitSet;
use arrayvec::ArrayVec;
use fnv::{FnvHashSet, FnvHashMap};

pub mod day1 {
    use super::*;

    pub fn generator(input: &str) -> Vec<i32> {
        input.lines().map(|x| x.parse().expect("Not an integer")).sorted().collect()
    }

    pub fn part1(input: &[i32]) -> i32 {
        let target = 2020;

        let set : FnvHashSet<i32> = input.iter().copied().collect();

        input.iter().find_map(|int| {
            let remainder = target - int;

            set.get(&remainder).and(Some(remainder * int))
        }).expect("Must exist")
    }

    pub fn part2(input: &[i32]) -> i32 {
        let target = 2020;

        let set : FnvHashSet<i32> = input.iter().copied().collect();

        for (i, xi) in input.iter().enumerate() {
            for xj in input.iter().skip(i) {
                let remainder = target - xi - xj;
                if remainder <= 0 {
                    break;
                }
                if set.contains(&remainder) {
                    return remainder * xi * xj;
                }
            }
        }

        unreachable!();
    }
}

pub mod day2 {
    use super::*;

    pub struct Day2<'a> {
        low: usize,
        high: usize,
        c: char,
        password: &'a str,
    }

    pub fn generator(input: &str) -> Vec<Day2> {
        input.lines().map(|x| {
            // {low}-{high} {c}: {password}
            let dash = x.find('-').expect("No dash");
            let space = x.find(' ').expect("No space");

            Day2 {
                low: x[0..dash].parse().expect("Invalid low"),
                high: x[dash+1..space].parse().expect("Invalid high"),
                c: x.as_bytes()[space+1] as char,
                password: &x[space+4..],
            }
        }).collect()
    }

    pub fn part1(input: &[Day2]) -> usize {
        input.iter().filter(|entry| {
            let matching = entry.password.chars().filter(|&c| c == entry.c).count();

            matching >= entry.low && matching <= entry.high
        }).count()
    }

    pub fn part2(input: &[Day2]) -> usize {
        input.iter().filter(|entry| {
            let first = entry.password.as_bytes().get(entry.low - 1).map_or(false, |&x| x as char == entry.c);
            let second = entry.password.as_bytes().get(entry.high - 1).map_or(false, |&x| x as char == entry.c);

            first ^ second
        }).count()
    }
}

pub mod day3 {
    use super::*;

    pub fn generator(input: &str) -> Vec<Vec<bool>> {
        input.lines().map(|line| line.chars().map(|c| c != '.').collect()).collect()
    }

    pub fn part1(input: &[Vec<bool>]) -> usize {
        input.iter().enumerate().filter(|(i, row)| row[3 * i % row.len()]).count()
    }

    pub fn part2(input: &[Vec<bool>]) -> usize {
        [(1,1), (3,1), (5,1), (7,1), (1,2)].iter().map(|&(right, down)|
           input.iter().step_by(down).enumerate().filter(|(i, row)| row[right * i % row.len()]).count()
        ).product()
    }
}


pub mod day4 {
    use super::*;

    pub struct Day4<'a> {
        byr: Option<&'a str>,
        iyr: Option<&'a str>,
        eyr: Option<&'a str>,
        hgt: Option<&'a str>,
        hcl: Option<&'a str>,
        ecl: Option<&'a str>,
        pid: Option<&'a str>,
    }

    pub fn generator(input: &str) -> Vec<Day4> {
        let passports : Vec<&str> = input.lines().collect();

        passports.split(|&x| x == "").map(|pass| {
            let mut tmp = Day4 {
                byr: None,
                iyr: None,
                eyr: None,
                hgt: None,
                hcl: None,
                ecl: None,
                pid: None,
            };
            for line in pass {
                for elem in line.split(' ') {
                    let (key, value) = elem.split_at(elem.find(':').expect("Invalid key-value format"));
                    match key {
                        "byr" => tmp.byr = Some(value),
                        "iyr" => tmp.iyr = Some(value),
                        "eyr" => tmp.eyr = Some(value),
                        "hgt" => tmp.hgt = Some(value),
                        "hcl" => tmp.hcl = Some(value),
                        "ecl" => tmp.ecl = Some(value),
                        "pid" => tmp.pid = Some(value),
                        "cid" => (),
                        _ => unreachable!()
                    }
                }
            }

            tmp
        }).collect()
    }

    pub fn part1(input: &[Day4]) -> usize {
        input.iter().filter(|p| {
            p.byr.is_some() &&
            p.iyr.is_some() &&
            p.eyr.is_some() &&
            p.hgt.is_some() &&
            p.hcl.is_some() &&
            p.ecl.is_some() &&
            p.pid.is_some()
        }).count()
    }

    pub fn part2(input: &[Day4]) -> usize {
        let hgt_re = Regex::new(r"^(\d+)(cm|in)$").expect("Invalid regex");
        let hgt_v = |hgt| {
            match hgt_re.captures(hgt) {
                Some(captures) =>
                    match &captures[2] {
                        "cm" => captures[1].parse::<u32>().ok().map_or(false, |x| x >= 150 && x <= 193),
                        "in" => captures[1].parse::<u32>().ok().map_or(false, |x| x >= 59 && x <= 76),
                        _ => unreachable!()
                    }
                None => false
            }
        };
        let hcl_re = Regex::new(r"^#[0-9a-f]{6}$").expect("Invalid regex");
        let ecl_re = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$").expect("Invalid regex");
        let pid_re = Regex::new(r"^\d{9}$").expect("Invalid regex");

        input.iter().filter(|p| {
            p.byr.map_or(false, |byr| byr.parse::<u32>().ok().map_or(false, |year| year >= 1920 && year <= 2002)) &&
            p.iyr.map_or(false, |iyr| iyr.parse::<u32>().ok().map_or(false, |year| year >= 2010 && year <= 2020)) &&
            p.eyr.map_or(false, |eyr| eyr.parse::<u32>().ok().map_or(false, |year| year >= 2020 && year <= 2030)) &&
            p.hgt.map_or(false, hgt_v) &&
            p.hcl.map_or(false, |hcl| hcl_re.is_match(hcl)) &&
            p.ecl.map_or(false, |ecl| ecl_re.is_match(ecl)) &&
            p.pid.map_or(false, |pid| pid_re.is_match(pid))
        }).count()
    }
}

pub mod day5 {
    use super::*;

    pub fn generator(input: &str) -> Vec<usize> {
        input.lines().map(|line| {
            let (ticket_row, ticket_column) = line.split_at(7);
            let row = ticket_row.chars().map(|c| c == 'B').fold(0, |acc, x| (acc << 1) + (x as usize));
            let column = ticket_column.chars().map(|c| c == 'R').fold(0, |acc, x| (acc << 1) + (x as usize));

            row * 8 + column
        }).collect()
    }

    pub fn part1(input: &[usize]) -> usize {
        *input.iter().max().expect("Nonempty")
    }

    pub fn part2(input: &[usize]) -> usize {
        let all_seats : BitSet = input.iter().copied().collect();

        input.into_iter().filter(|&t| !all_seats.contains((t + 1) as usize)).filter(|&t| t % 8 != 7).find(|&t| all_seats.contains((t + 2) as usize)).expect("Must be one") + 1
    }
}

pub mod day6 {
    use super::*;

    pub fn generator(input: &str) -> Vec<Vec<u32>> {
        let groups = input.lines().collect::<Vec<&str>>();

        groups.split(|&x| x == "").map(|group|
            group.iter().map(|line| line.bytes().fold(0, |acc, c| acc | (1 << (c - b'a')))).collect()
        ).collect()
    }

    pub fn part1(groups: &[Vec<u32>]) -> u32 {
        groups.iter().map(|sets| {
            sets.iter().fold(0, |acc, x| acc | x).count_ones()
        }).sum()
    }

    pub fn part2(groups: &[Vec<u32>]) -> u32 {
        groups.iter().map(|sets| {
            sets.iter().fold(std::u32::MAX, |acc, set| acc & set).count_ones()
        }).sum()
    }
}

pub mod day7 {
    use super::*;

    #[derive(Debug)]
    pub struct Day7<'a> {
        color: &'a str,
        contains: Vec<(usize, &'a str)>
    }

    pub fn generator(input: &str) -> Vec<Day7> {
        input.lines().map(|line| {
            let header_index = line.find(" contain ").expect("Invalid bag record");

            Day7 {
                color: line[0..header_index].strip_suffix(" bags").expect("No suffix"),
                contains: line[header_index + " contain ".len()..].split(", ").filter_map(|bag_info| {
                    if bag_info.starts_with("no other") {
                        None
                    } else {
                        let first_space = bag_info.find(' ').expect("No first space");
                        let last_space = bag_info.rfind(' ').expect("No last space");
                        Some((
                            bag_info[0..first_space].parse::<usize>().expect("Invalid integer"),
                            &bag_info[first_space+1..last_space]
                        ))
                    }
                }).collect()
            }
        }).collect()
    }

    pub fn part1(bags: &[Day7]) -> usize {
        let target = "shiny gold";
        let contained = {
            let mut contained = FnvHashMap::<&str, Vec<&str>>::default();

            for bag in bags.iter() {
                for (_, color) in bag.contains.iter() {
                    contained.entry(color).and_modify(|vec| vec.push(bag.color)).or_insert_with(|| vec![bag.color]);
                }
            }
            contained
        };

        fn recur<'a, H:BuildHasher>(curr: &'a str, contained: &HashMap<&'a str, Vec<&'a str>, H>, mut visited: &mut HashSet<&'a str, H>) -> usize {
            if !visited.insert(curr) {
                return 0;
            }

            contained.get(curr).map_or(0, |v| v.iter().map(|x| recur(x, contained, &mut visited)).sum()) + 1
        }

        recur(target, &contained, &mut FnvHashSet::default()) - 1 // Don't count ourselves
    }

    pub fn part2(bags: &[Day7]) -> usize {
        let target = "shiny gold";
        let contains : FnvHashMap<&str, &Day7> = bags.iter().map(|bag| (bag.color, bag)).collect();

        // We don't bother memoizing because it's fast enough
        fn recur<H:BuildHasher>(curr: &str, contains: &HashMap<&str, &Day7, H>) -> usize {
            contains.get(curr).expect("Must be a bag record").contains.iter().map(|(count, color)| count * recur(color, contains)).sum::<usize>() + 1
        }

        // Don't count ourselves
        recur(target, &contains) - 1
    }
}

pub mod day8 {
    use super::*;

    #[derive(Debug, FromStr, PartialEq, Eq, Clone)]
    #[display(style = "lowercase")]
    pub enum Day8Instruction {
        Acc,
        Jmp,
        Nop
    }
    #[derive(Debug, FromStr, Clone)]
    #[display("{ins} {offset}")]
    pub struct Day8 {
        ins: Day8Instruction,
        offset: i32
    }

    pub enum Day8Execution {
        /// This execution pass halted, returning the given accumulator
        Halted(i32),
        /// This execution pass looped, with the given accumulator at the point where the looping was
        /// detected.
        Loopy(i32)
    }

    // Returns the accumulator after the instructions either start looping or terminate.
    pub fn interpret_day8instructions(instructions: &[Day8]) -> Day8Execution {
        let mut instructions_hit = BitSet::new();
        let mut acc = 0;
        let mut ip = 0;

        loop {
            if ip == instructions.len() {
                return Day8Execution::Halted(acc);
            }
            if !instructions_hit.insert(ip) {
                return Day8Execution::Loopy(acc);
            }

            ip += match &instructions[ip] {
                Day8{ins: Day8Instruction::Acc, offset: off} => {
                    acc += off;
                    1
                },
                Day8{ins: Day8Instruction::Jmp, offset: off} => *off as usize,
                Day8{ins: Day8Instruction::Nop, offset: _} => 1,
            };
        }
    }

    pub fn generator(input: &str) -> Vec<Day8> {
        input.lines().map(|line| line.parse::<Day8>().expect("Invalid instruction")).collect()
    }

    pub fn part1(instructions: &[Day8]) -> i32 {
        match interpret_day8instructions(instructions) {
            Day8Execution::Loopy(acc) => acc,
            _ => unreachable!()
        }
    }

    pub fn part2(instructions: &[Day8]) -> i32 {
        instructions.iter().enumerate().filter(|(_, ins)| {
            ins.ins != Day8Instruction::Acc
        }).find_map(|(modified, ins)| {
            let mut mutated = instructions.to_vec();

            mutated[modified].ins = if ins.ins == Day8Instruction::Jmp {
                 Day8Instruction::Nop
            } else {
                Day8Instruction::Jmp
            };

            match interpret_day8instructions(&mutated) {
                Day8Execution::Loopy(_) => None,
                Day8Execution::Halted(acc) => Some(acc)
            }
        }).expect("Must be a mutated instruction")
    }
}

pub mod day9 {
    use super::*;

    pub fn generator(input: &str) -> Vec<usize> {
        input.lines().map(|x| x.parse().expect("Non-integer")).collect()
    }

    pub fn part1(nums: &[usize]) -> usize {
        let window = 25;

        *nums.iter().enumerate().skip(window + 1).find_map(|(i,target)| {
            let range = &nums[i-window..i];

            let summed = range.iter().enumerate().find(|&(start, x)|
                range.iter().skip(start).any(|y| x != y && x + y == *target)
            );

            match summed {
                None => Some(target),
                _ => None
            }
        }).expect("No invalid numbers")
    }

    pub fn part2(nums: &[usize]) -> usize {
        let invalid = part1(nums);

        let mut start = 0;
        let mut end = 0;
        let mut running_sum = nums[0];

        let subset = loop {
            running_sum = if running_sum < invalid {
                end += 1;
                running_sum + nums[end]
            } else {
                start += 1;
                running_sum - nums[start - 1]
            };

            if running_sum == invalid {
                break &nums[start..end+1];
            }
            assert!(start < end, "Start passed end");
        };

        let (min, max) = subset.iter().fold((std::usize::MAX, 0), |(acc_min, acc_max), &x| (cmp::min(acc_min, x), cmp::max(acc_max, x)));
        min + max
    }
}


pub mod day10 {
    use super::*;

    pub fn generator(input: &str) -> Vec<usize> {
        input.lines().map(|x| x.parse().expect("Non-integer")).sorted().collect()
    }

    pub fn part1(adapters: &[usize]) -> usize {
        // The diff between the device and the highest adapter is 3
        let mut diffs = vec![0, 0, 0, 1];

        // Compute the diff between the outlet and the first adapter.
        diffs[adapters[0]] = 1;

        for (a,b) in adapters.iter().zip(adapters.iter().skip(1)) {
            diffs[b - a] += 1;
        }

        diffs[1] * diffs[3]
    }

    pub fn part2(adapters: &[usize]) -> usize {
        let end = *adapters.iter().max().expect("No adapters");

        // Since a vector can be no more than 3x as large as the corresponding hashtable, we use it
        // for speed.
        let mut table = Vec::<Option<usize>>::new();
        table.resize(end + 1, None);
        table[end] = Some(1);

        pub fn dp(current: usize, remaining_adapters: &[usize], mut table: &mut Vec<Option<usize>>) -> usize {
            // The number of pathways reachable from the current state is the sum of the number of
            // pathways reachable from any next state.
            let total = remaining_adapters.iter().take_while(|&x| x - current <= 3).copied().enumerate().map(|(i, adapter)| {
                match table[adapter] {
                    Some(total) => total,
                    None => dp(adapter, &remaining_adapters[i+1..], &mut table)
                }
            }).sum::<usize>();

            table[current] = Some(total);
            total
        }

        dp(0, adapters, &mut table)
    }
}


pub mod day11 {
    use super::*;

    pub fn generator(input: &str) -> Vec<Vec<char>> {
        // We'll represent seat indices as u16 for better cache efficiency.
        assert!(input.len() < std::u16::MAX as usize, "Too many seats for a u16");

        input.lines().map(|x| x.chars().collect()).collect()
    }

    const MAX_NEIGHBORS: usize = 8;
    const ADJACENTS : [(isize, isize); MAX_NEIGHBORS] = [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];

    type NeighborTable = (u16, ArrayVec<u16, MAX_NEIGHBORS>);

    fn run_simulation(mut seats: Vec<char>, mut neighbors: Vec<NeighborTable>, too_many: usize) -> Vec<char> {
        let mut changes = Vec::<u16>::new();

        loop {
            // Note that anything that doesn't change after a given iteration will never change.
            // We track changes in a changelist for increased cache efficiency
            neighbors.retain(|(i, nearby)| {
                match seats[*i as usize] {
                    'L' if !nearby.iter().copied().any(|s| seats[s as usize] == '#') => {
                        changes.push(*i);
                        true
                    },
                    '#' if nearby.iter().copied().filter(|&s| seats[s as usize] == '#').take(too_many).count() == too_many => {
                        changes.push(*i);
                        true
                    },
                    _ => false
                }
            });

            if changes.is_empty() {
                return seats;
            }

            for i in changes.drain(..) {
                seats[i as usize] = match seats[i as usize] {
                    'L' => '#',
                    '#' => 'L',
                    _ => unreachable!()
                };
            }
        }
    }

    pub fn part1(initial_seats: &[Vec<char>]) -> usize {
        let width = initial_seats[0].len();
        let seats : Vec<char> = initial_seats.iter().flatten().copied().collect();

        let neighbors = seats.chunks(width).enumerate().map(|(y, row)| {
            row.iter().enumerate().filter(|(_, &s)| s != '.').filter_map(|(x,_)| {
                let vision = |(off_x, off_y)|
                    initial_seats.get(y + off_y as usize).and_then(|row| row.get(x + off_x as usize))
                    .and(Some(((y + off_y as usize) * width + (x + off_x as usize)) as u16));

                let neighboring = ADJACENTS.iter().copied().filter_map(vision).sorted().collect::<ArrayVec<u16, MAX_NEIGHBORS>>();

                match neighboring.len() {
                    0 => None,
                    _ => Some(((y * width + x) as u16, neighboring))
                }
            }).collect::<Vec<NeighborTable>>()
        }).flatten().collect::<Vec<NeighborTable>>();

        let seats = run_simulation(seats, neighbors, 4);
        seats.iter().copied().filter(|&s| s == '#').count()
    }

    pub fn part2(initial_seats: &[Vec<char>]) -> usize {
        let width = initial_seats[0].len();

        let seats = initial_seats.iter().flatten().copied().collect::<Vec<char>>();
        let neighbors = seats.chunks(width).enumerate().map(|(y, row)| {
            row.iter().enumerate().filter(|(_, &s)| s != '.').filter_map(|(x,_)| {
                let vision = |(off_x, off_y)| {
                    // Run a step in every direction until we hit the edge of the plane.
                    (1 as isize..).take_while(|&step|
                        initial_seats.get(y + (step * off_y) as usize).and_then(|row| row.get(x + (step * off_x) as usize)).is_some()
                    ).map(|step| (x + (step * off_x) as usize, y + (step * off_y) as usize))
                    .map(|(x,y)| (y * width + x) as u16)
                    // Find the first valid seat.
                    .find(|&i| seats[i as usize] != '.')
                };

                let neighboring = ADJACENTS.iter().copied().filter_map(vision).sorted().collect::<ArrayVec<u16, MAX_NEIGHBORS>>();

                match neighboring.len() {
                    0 => None,
                    _ => Some(((y * width + x) as u16, neighboring))
                }
            }).collect::<Vec<NeighborTable>>()
        }).flatten().collect::<Vec<NeighborTable>>();

        let seats = run_simulation(seats, neighbors, 5);
        seats.into_iter().filter(|&s| s == '#').count()
    }
}


pub mod day12 {
    use super::*;

    #[derive(Debug, FromStr, Clone, Copy)]
    #[display(style = "UPPERCASE")]
    pub enum Direction {
        N,
        S,
        E,
        W,
        L,
        R,
        F
    }

    #[derive(Debug)]
    pub struct Instruction {
        dir: Direction,
        val: isize,
    }

    pub fn generator(input: &str) -> Vec<Instruction> {
        input.lines().map(|x| Instruction {
            dir: x[..1].parse().expect("Not a direction"),
            val: x[1..].parse().expect("Non-integer")
        }).collect()
    }

    pub fn part1(instructions: &[Instruction]) -> isize {
        let coord = instructions.iter().fold((0, 0, 0), |(pos_x, pos_y, heading), ins| {
            match &ins.dir {
                Direction::E => (pos_x + ins.val, pos_y, heading),
                Direction::N => (pos_x, pos_y + ins.val, heading),
                Direction::S => (pos_x, pos_y - ins.val, heading),
                Direction::W => (pos_x - ins.val, pos_y, heading),
                Direction::F => match heading {
                    0 => (pos_x + ins.val, pos_y, heading),
                    90 => (pos_x, pos_y + ins.val, heading),
                    180 => (pos_x - ins.val, pos_y, heading),
                    270 => (pos_x, pos_y - ins.val, heading),
                    _ => unreachable!()
                },
                Direction::L => (pos_x, pos_y, (heading + ins.val).rem_euclid(360)),
                Direction::R => (pos_x, pos_y, (heading - ins.val).rem_euclid(360)),
            }
        });

        coord.0.abs() + coord.1.abs()
    }

    pub fn part2(instructions: &[Instruction]) -> isize {
        let mut ship: (isize, isize) = (0, 0);

        instructions.iter().fold((10, 1), |diff, ins| {
            match &ins.dir {
                // Bump the waypoint
                Direction::N => (diff.0, diff.1 + ins.val),
                Direction::S => (diff.0, diff.1 - ins.val),
                Direction::E => (diff.0 + ins.val, diff.1),
                Direction::W => (diff.0 - ins.val, diff.1),

                // Move the ship to the waypoint
                Direction::F => {
                    ship = (ship.0 + diff.0 * ins.val, ship.1 + diff.1 * ins.val);
                    diff
                }

                // Swing the waypoint around the ship
                &rotate => {
                    match (ins.val, rotate) {
                        (90, Direction::L) | (270, Direction::R) => (-diff.1, diff.0),
                        (90, Direction::R) | (270, Direction::L) => (diff.1, -diff.0),
                        (180, _) => (-diff.0, -diff.1),

                        _ => panic!("Unexpected angle"),
                    }
                }
            }
        });

        ship.0.abs() + ship.1.abs()
    }
}

pub mod day13 {
    use super::*;

    pub fn generator(input: &str) -> (usize, Vec<(usize, usize)>) {
        let earliest = input.lines().next().expect("No timestamp line").parse().expect("Invalid timestamp");
        let schedule = input.lines().nth(1).expect("No schedule line");

        (earliest, schedule.split(',').enumerate().filter(|(_, bus)| bus != &"x").map(|(offset, bus)|
            (offset, bus.parse().expect("Invalid bus id"))
        ).collect())
    }

    pub fn part1((earliest, buses): &(usize, Vec<(usize, usize)>)) -> usize {
        let (bus, ts) = buses.iter().map(|(_, bus)| (bus, (((earliest-1) / bus) + 1)* bus)).min_by_key(|&(_, ts)| ts).expect("No buses");

        bus * (ts - earliest)
    }

    // List of (residue a_i, modulus n_i) pairs, where the moduli are coprime.
    fn chinese_remainder(pairs: &[(isize, isize)]) -> isize {
        let prod = pairs.iter().map(|(_, n_i)| n_i).product::<isize>();

        // Given p and n_i, compute r_i/s_i such that r_i*n_i + s_i*p = 1.
        fn mod_inv(p: isize, n_i: isize) -> isize {
            fn extended_euclidean(a: isize, b: isize) -> (isize, isize, isize) {
                if a == 0 {
                    (b, 0, 1)
                } else {
                    let (g, x, y) = extended_euclidean(b % a, a);
                    (g, y - (b / a) * x, x)
                }
            }

            let (g, s_i, _) = extended_euclidean(p, n_i);
            assert!(g == 1,  "Moduli not pairwise coprime");

            s_i.rem_euclid(n_i)
        }

        let sum = pairs.iter().fold(0, |acc, &(a_i, n_i)| {
            // prod/n_i is coprime
            let p = prod / n_i;

            // x = \sum_i of a_i * s_i * prod/n_i
            acc + a_i * p * mod_inv(p, n_i)
        });

        sum % prod
    }

    pub fn part2((_, schedule): &(usize, Vec<(usize, usize)>)) -> isize {
        // We're given offsets from ts, but we really want the remainder from the bus.
        let crt_input = schedule.iter().map(|(offset, bus)| ((*bus - *offset) as isize, *bus as isize)).collect::<Vec<(isize, isize)>>();

        chinese_remainder(&crt_input)
    }
}


pub mod day14 {
    use super::*;

    pub enum Instruction<'a> {
        Mask(&'a str),
        Assign(u64, u64)
    }

    pub type ProblemInput<'a> = Vec<Instruction<'a>>;

    pub fn generator(input: &str) -> ProblemInput {
        input.lines().map(|line| {
            let assignment = line.find(" = ").expect("No assign");

            match &line[0..4] {
                "mask" => Instruction::Mask(&line[assignment+3..]),
                "mem[" => Instruction::Assign(
                    line[4..assignment-1].parse().expect("Invalid addr"),
                    line[assignment + 3..].parse().expect("Bad value")
                ),
                _ => panic!("Invalid line")
            }
        }).collect()
    }

    type Memory = FnvHashMap::<u64, u64>;

    pub fn part1(input: &ProblemInput) -> u64 {
        let mut and_mask = 0;
        let mut or_mask = 0;

        let mut memory = Memory::default();

        for ins in input.iter() {
            match &ins {
                Instruction::Mask(mask) => {
                    or_mask = 0;
                    and_mask = 0;

                    for (i, c) in mask.chars().rev().enumerate() {
                        match c {
                            '0' => {
                                and_mask |= 1 << i;
                            },
                            '1' => {
                                or_mask |= 1 << i;
                            },
                            _ => ()
                        }
                    }
                },
                Instruction::Assign(addr, val) => {
                    memory.insert(*addr as u64, (val & !and_mask) | or_mask);
                }
            }
        }

        memory.values().copied().sum()
    }


    pub fn part2(input: &ProblemInput) -> u64 {
        let mut and_mask = std::u64::MAX;
        let mut or_mask = 0;
        let mut floating = ArrayVec::<u8, 32>::new();

        let mut memory = Memory::default();

        for ins in input.iter() {
            match ins {
                Instruction::Mask(mask) => {
                    or_mask = 0;
                    and_mask = 0;
                    floating.clear();

                    for (i, c) in mask.chars().rev().enumerate() {
                        match c {
                            '0' => (),
                            '1' => {
                                or_mask |= 1 << i;
                            },
                            'X' => {
                                and_mask |= 1 << i;
                                floating.push(i as u8);
                            },
                            _ => panic!("Invalid mask")
                        }
                    }
                },
                Instruction::Assign(addr, val) => {
                    let addr_mask = (*addr as u64 & !and_mask) | or_mask;

                    fn fork(memory: &mut Memory, remaining_floats: &[u8], addr_mask: u64, val: u64) {
                        match remaining_floats.get(0) {
                            Some(i) => {
                                fork(memory, &remaining_floats[1..], addr_mask, val);
                                fork(memory, &remaining_floats[1..], addr_mask | (1 << i), val);
                            },
                            None => {
                                memory.insert(addr_mask, val);
                            }
                        }
                    }

                    fork(&mut memory, &floating, addr_mask, *val);
                }
            }
        }

        memory.values().copied().sum()
    }
}

pub mod day15 {
    use super::*;

    pub fn generator(input: &str) -> Vec<usize> {
        input.lines().next().expect("Must be a line").split(",").map(|x| x.parse().expect("Invalid integer")).collect()
    }

    pub fn part1(numbers: &[usize]) -> usize {
        const TARGET : usize = 2020;

        let mut last_spoken = [0 as u32; TARGET + 1];
        assert!(TARGET < std::u32::MAX as usize, "TARGET too large for u32");

        for (i, &x) in numbers.iter().enumerate() {
            last_spoken[x] = (i + 1) as u32;
        }

        (numbers.len()+1..TARGET+1).fold(numbers[numbers.len()-1], |prev, turn| {
            let turn = turn as u32;
            let last = last_spoken[prev];
            let next = if last == 0 { 0 } else { turn - 1 - last };

            last_spoken[prev] = turn - 1;
            next as usize
        })
    }

    pub fn part2(numbers: &[usize]) -> usize {
        const TARGET : usize = 30000000;

        let mut last_spoken = vec![0 as u32; TARGET + 1]; // Avoid stack overflow
        assert!(TARGET < std::u32::MAX as usize, "TARGET too large for u32");

        for (i, &x) in numbers.iter().enumerate() {
            last_spoken[x] = (i + 1) as u32;
        }

        (numbers.len()+1..TARGET+1).fold(numbers[numbers.len()-1], |prev, turn| {
            let turn = turn as u32;
            let last = last_spoken[prev];
            let next = if last == 0 { 0 } else { turn - 1 - last };

            last_spoken[prev] = turn - 1;
            next as usize
        })
    }
}

pub mod day16 {
    use super::*;
    use std::ops::RangeInclusive;

    #[derive(Debug)]
    pub struct Field<'a> {
        name: &'a str,
        a: RangeInclusive<u32>,
        b: RangeInclusive<u32>,
    }

    type Ticket = Vec<u32>;

    pub fn generator(input: &str) -> (Vec<Field>, Ticket, Vec<Ticket>) {
        const YOUR_TICKET: &str = "\nyour ticket:\n";
        const NEARBY_TICKET: &str = "\nnearby tickets:\n";
        let ticket_offset = input.find(YOUR_TICKET).expect("No ticket");
        let nearby_offset = input.find(NEARBY_TICKET).expect("No nearby ticket");

        let fields = input[0..ticket_offset].lines().map(|line| {
            let sep = line.find(": ").expect("No separator");
            let or = line.find(" or ").expect("No or");

            Field {
                name: &line[0..sep],
                a: {
                    let nums = line[sep+2..or].split('-').map(|x| x.parse().expect("Not an integer")).collect::<Vec<u32>>();
                    nums[0]..=nums[1]
                },
                b: {
                    let nums = line[or+4..].split('-').map(|x| x.parse().expect("Not an integer")).collect::<Vec<u32>>();
                    nums[0]..=nums[1]
                },
            }
        }).collect::<Vec<Field>>();

        let nearby_tickets = input[nearby_offset+NEARBY_TICKET.len()..].lines().map(|line| line.split(',').map(|x| x.parse().expect("Not an integer")).collect()).collect::<Vec<Ticket>>();

        let ticket = input[ticket_offset+YOUR_TICKET.len()..nearby_offset-1].split(',').map(|x| x.parse().expect("Not an integer")).collect::<Ticket>();

        assert!(fields.len() == ticket.len(), "Missing fields");
        (fields, ticket, nearby_tickets)
    }

    pub fn part1((fields, _, nearby): &(Vec<Field>, Ticket, Vec<Ticket>)) -> u32 {
        nearby.iter().map(|t| {
            t.iter().filter(|&tf| {
                !fields.iter().any(|field| {
                    field.a.contains(tf) || field.b.contains(tf)
                })
            }).sum::<u32>()
        }).sum::<u32>()
    }

    pub fn part2((fields, ticket, nearby): &(Vec<Field>, Ticket, Vec<Ticket>)) -> usize {
        let valid = nearby.iter().filter(|&t| {
            t.iter().all(|tf| {
                fields.iter().any(|field| {
                    field.a.contains(tf) || field.b.contains(tf)
                })
            })
        }).cloned().collect::<Vec<Ticket>>();

        // For each field, try each ticket against every column to see which might match.
        let mut options = fields.iter().map(|field| {
            let field_options = (0..fields.len()).filter(|&i|
                valid.iter().all(|t| field.a.contains(&t[i]) || field.b.contains(&t[i]))
            ).collect::<BitSet>();

            (field.name, field_options)
        }).collect::<Vec<(&str, BitSet)>>();

        let mut departure_product = 1;

        // We can remove one option at each round.  If we ever fail to remove one, we must be done.
        while let Some((name, fields)) = options.iter().find(|(_, fields)| fields.len() == 1) {
            let taken_field = fields.iter().next().expect("Length mismatch");

            if name.starts_with("departure") {
                departure_product *= ticket[taken_field] as usize;
            }

            for (_, valid) in options.iter_mut() {
                valid.remove(taken_field);
            };
        }

        departure_product
    }
}

pub mod day17 {
    use super::*;

    pub fn generator(input: &str) -> Vec<Vec<bool>> {
        input.lines().map(|line| line.chars().map(|x| x == '#').collect()).collect()
    }


    pub fn part1(seed: &[Vec<bool>]) -> usize {
        const ROUNDS: u8 = 6;

        // We can't grow by more than `rounds in any direction`, so we can use that for canvas
        // sizing to avoid needing to cast between isize/usize.
        // (x, y, z) with origin (rounds, rounds, rounds)
        type Coord = (u8, u8, u8);

        let mut active_cells = seed.iter().enumerate().map(move |(y, row)|
            row.iter().enumerate().filter(|(_, &cell)| cell).map(move |(x, _)|
                (x as u8 + ROUNDS, y as u8 + ROUNDS, 0 + ROUNDS)
            )
        ).flatten().collect::<FnvHashSet<Coord>>();

        let offsets = (0..3).map(|_| (-1 as isize..=1)).multi_cartesian_product().map(|c|
            (c[0] as u8, c[1] as u8, c[2] as u8)
        ).filter(|&c| (0,0,0) != c).collect::<Vec<Coord>>();

        for _ in 0..ROUNDS {
            let mut neighbors = FnvHashMap::<Coord, u32>::default();

            // Hit the neighbor of each active cell.
            for (x,y,z) in active_cells.iter() {
                for off in offsets.iter() {
                    let neighbor = (x + off.0, y + off.1, z + off.2);

                    neighbors.entry(neighbor).and_modify(|x| *x += 1).or_insert(1);
                }
            }

            // Cells must have been hit to be active, so for each cell that was hit check if
            // they should be activated.
            let next_active = neighbors.iter().filter_map(|(c, hits)| match hits {
                3 => Some(c),
                2 if active_cells.contains(c) => Some(c),
                _ => None,
            }).copied().collect::<FnvHashSet<Coord>>();

            active_cells = next_active;
        }

        active_cells.iter().count()
    }

    pub fn part2(seed: &[Vec<bool>]) -> usize {
        const ROUNDS: u8 = 6;

        // We can't grow by more than `rounds in any direction`, so we can use that for canvas
        // sizing to avoid needing to cast between isize/usize.
        // (x, y, z, w) with origin (rounds, rounds, rounds, rounds)
        type Coord = (u8, u8, u8, u8);

        let mut active_cells = seed.iter().enumerate().map(move |(y, row)|
            row.iter().enumerate().filter(|(_, &cell)| cell).map(move |(x, _)|
                (x as u8 + ROUNDS, y as u8 + ROUNDS, 0 + ROUNDS, 0 + ROUNDS)
            )
        ).flatten().collect::<FnvHashSet<Coord>>();

        let offsets = (0..4).map(|_| (-1 as isize..=1)).multi_cartesian_product().map(|c|
            (c[0] as u8, c[1] as u8, c[2] as u8, c[3] as u8)
        ).filter(|&c| (0,0,0,0) != c).collect::<Vec<Coord>>();

        for _ in 0..ROUNDS {
            let mut neighbors = FnvHashMap::<Coord, u32>::default();

            // Hit the neighbor of each active cell.
            for (x,y,z,w) in active_cells.iter() {
                for off in offsets.iter() {
                    let neighbor = (x + off.0, y + off.1, z + off.2, w + off.3);

                    neighbors.entry(neighbor).and_modify(|x| *x += 1).or_insert(1);
                }
            }

            // Cells must have been hit to be active, so for each cell that was hit check if
            // they should be activated.
            let next_active = neighbors.iter().filter_map(|(c, hits)| match hits {
                3 => Some(c),
                2 if active_cells.contains(c) => Some(c),
                _ => None,
            }).copied().collect::<FnvHashSet<Coord>>();

            active_cells = next_active;
        }

        active_cells.iter().count()
    }
}

pub mod day18 {
    use super::*;

    pub fn generator(input: &str) -> Vec<&str> {
        input.lines().collect()
    }

    enum Op {
        Add,
        Mul
    }

    pub fn part1(input: &[&str]) -> usize {
        // Returns (result, bytes parsed)
        fn parse_expr(range: &[u8]) -> (usize, usize) {
            let mut val = 0;
            let mut op = Op::Add;

            let mut i = 0;
            while i < range.len() {
                let c = range[i];

                match c as char {
                    '*' => op = Op::Mul,
                    '+' => op = Op::Add,
                    ')' => return (val, i + 1), // Terminate this parse after the close paren.
                    '0'..='9' => match op {
                        Op::Mul => val *= (c - b'0') as usize,
                        Op::Add => val += (c - b'0') as usize,
                    },
                    '(' => {
                        let (next, end) = parse_expr(&range[i+1..]);
                        i += end;

                        match op {
                            Op::Mul => val *= next,
                            Op::Add => val += next,
                        }
                    },
                    _ => (), // Skip whitespace
                }

                i += 1;
            }

            (val, range.len())
        }

        input.iter().map(|line| parse_expr(line.as_bytes()).0).sum()
    }

    pub fn part2(input: &[&str]) -> usize {
        fn parse_expr(range: &[u8]) -> usize {
            let mut val = 0;

            let mut i = 0;
            while i < range.len() {
                let c = range[i];

                match c as char {
                    '*' => {
                        // Mul has lower precedence than anything else, so parse everything left first.
                        val *= parse_expr(&range[i+1..]);
                        break;
                    },
                    '+' => (),  // Keep accumulating
                    '0'..='9' => val += (c - b'0') as usize,
                    ')' => return val,
                    '(' => {
                        // Find the corresponding close paren and evaluate to it.
                        let end = range.iter().enumerate().skip(i+1).try_fold(1, |opens, (j, c)| {
                            match (opens, c) {
                                (_, b'(') => Ok(opens + 1),
                                (1, b')') => Err(j),
                                (_, b')') => Ok(opens - 1),
                                _ => Ok(opens)
                            }
                        }).expect_err("No closing paren");

                        val += parse_expr(&range[i+1..end]);
                        i = end;
                    },
                    _ => (),  // Noop
                }

                i += 1;
            }

            val
        }

        input.iter().map(|line| parse_expr(line.as_bytes())).sum()
    }
}

pub mod day19 {
    use super::*;

    type Ref = u32;

    #[derive(Clone)]
    pub enum Rule<'a> {
        Terminal(&'a str),
        Refs(Vec<Vec<Ref>>),
    }

    pub fn generator(input: &str) -> (FnvHashMap<Ref, Rule>, &str) {
        let rules_end = input.find("\n\n").expect("No inputs");
        let (rules, inputs) = input.split_at(rules_end);

        (
            rules.lines().map(|line| {
                let colon = line.find(':').expect("No rule definition");

                let rule_num = line[..colon].parse().expect("Invalid rule id");
                let rule = line[colon+1..].trim();

                (
                    rule_num,
                    if rule.starts_with('"') {
                        // Must be a terminal string
                        Rule::Terminal(&rule[1..rule.len()-1])
                    } else {
                        Rule::Refs(rule.split('|').map(|subrule|
                            subrule.trim().split(' ').map(|id| id.parse().expect("Invalid ref")).collect()
                        ).collect())
                    }
                )
            }).collect(),
            inputs.trim()
        )
    }

    fn matches<H:BuildHasher>(refs: &[Ref], target: &str, rule_table: &HashMap<Ref, Rule, H>) -> bool {
        match refs {
            [] => target.len() == 0,
            [id, remaining_refs @ ..] => {
                match rule_table.get(&id).expect("No rule for ref") {
                    // If we found a terminator here, try to match the next rules vs the rest
                    // of the string.
                    Rule::Terminal(c) => target.starts_with(c) && matches(remaining_refs, &target[c.len()..], rule_table),
                    // Otherwise, hope we can match these rules too.
                    Rule::Refs(rules) => rules.iter().any(|subrule| {
                        let new_refs = subrule.iter().copied().chain(remaining_refs.iter().copied()).collect::<Vec<Ref>>();
                        matches(&new_refs, target, rule_table)
                    })
                }
            }
        }
    }

    pub fn part1<H:BuildHasher>((rules, inputs): &(HashMap<Ref, Rule, H>, &str)) -> usize {
        inputs.lines().filter(|&target| matches(&[0], target, &rules)).count()
    }

    pub fn part2<H:BuildHasher>((rules, inputs): &(HashMap<Ref, Rule, H>, &str)) -> usize {
        let rules = {
            // We're supposed to modify the input.
            let mut new = rules.iter().map(|(k,v)| (*k, v.clone())).collect::<FnvHashMap<Ref, Rule>>();

            new.insert(8, Rule::Refs(vec![vec![42], vec![42, 8]]));
            new.insert(11, Rule::Refs(vec![vec![42, 31], vec![42, 11, 31]]));
            new
        };

        inputs.lines().filter(|&target| matches(&[0], target, &rules)).count()
    }
}

pub mod day20 {
    use super::*;

    type Tile = usize;
    type TileRow = ArrayVec<bool, 10>;
    type TileData = Vec<TileRow>;

    pub fn generator(input: &str) -> FnvHashMap<Tile, TileData> {
        input.trim().split("\n\n").map(|x| {
            let mut lines = x.lines();

            let id = lines.next().expect("No tile id");

            (
                id["Tile ".len()..id.len()-1].parse().expect("Invalid tile id"),
                lines.map(|line| line.bytes().map(|c| c == b'#').collect()).collect()
            )
        }).collect()
    }

    pub fn part1<H:BuildHasher>(tiles: &HashMap<Tile, TileData, H>) -> usize {
        let mut side_lookup = FnvHashMap::<TileRow, Tile>::default();
        let mut matches = FnvHashMap::<Tile, FnvHashSet<Tile>>::default();

        for (&tile, data) in tiles.iter() {
            // Find the four sides,
            let mut find_matches = |side| {
                if let Some(prev) = side_lookup.insert(side, tile) {
                    matches.entry(tile).and_modify(|x| {x.insert(prev);}).or_insert([prev].iter().copied().collect());
                    matches.entry(prev).and_modify(|x| {x.insert(tile);}).or_insert([tile].iter().copied().collect());
                }
            };

            find_matches(data[0].clone());
            find_matches(data[data.len()-1].clone());
            find_matches(data.iter().map(|row| row[0]).collect());
            find_matches(data.iter().map(|row| row[row.len()-1]).collect());

            find_matches(data[0].iter().copied().rev().collect());
            find_matches(data[data.len()-1].iter().copied().rev().collect());
            find_matches(data.iter().rev().map(|row| row[0]).collect());
            find_matches(data.iter().rev().map(|row| row[row.len()-1]).collect());
        }

        let corners = matches.iter().filter(|(_,edges)| edges.len() == 2).map(|(tile, _)| *tile).collect::<FnvHashSet<Tile>>();

        corners.iter().product()
    }

    pub fn part2<H:BuildHasher>(_: &HashMap<Tile, TileData, H>) -> &'static str {
        "skipped - too much like work"
    }
}

pub mod day21 {
    use super::*;

    type IngredientName<'a> = &'a str;
    type Ingredient = usize;
    type Allergen = *const str;
    type Recipe = (FnvHashSet<Ingredient>, FnvHashSet<Allergen>);

    pub fn generator(input: &str) -> (Vec<Recipe>, FnvHashMap<IngredientName, Ingredient>) {
        const ALLERGEN_TEXT: &str = " (contains ";

        // Since there aren't many ingredients, we represent them as ids rather than strings to
        // allow us to work with BitSets.
        let mut ingredient_ids = FnvHashMap::<IngredientName, Ingredient>::default();
        let mut gid: Ingredient = 0;

        let mut allergen_id = FnvHashSet::<&str>::default();

        let recipes = input.lines().map(|line| {
            let allergens_id = line.find(ALLERGEN_TEXT).expect("No allergens");

            (
                line[..allergens_id].split(' ').map(|ingredient| {
                    // Convert each ingredient into an id
                    *ingredient_ids.entry(ingredient).or_insert_with(|| {
                        gid += 1;
                        gid - 1
                    })
                }).collect(),
                line[allergens_id+ALLERGEN_TEXT.len()..line.len()-1].split(", ").map(|x| {
                    // We never use allergens as anything but keys in a hashtable and don't operate
                    // on them except as strings.  Rather than storing them directly as strings and
                    // dealing with string equality, we enforce that they're generated uniquely and
                    // store them as the pointer instead.
                    (match allergen_id.get(x) {
                        Some(&id) => id,
                        None => {
                            allergen_id.insert(x);
                            x
                        }
                    }) as *const str
                }).collect()
            )
        }).collect();

        (recipes, ingredient_ids)
    }

    // Maps allergens to the set of potential ingredients for that allergen.
    fn potential_allergens(recipes: &[Recipe]) -> FnvHashMap<Allergen, BitSet> {
        let mut allergens = FnvHashMap::<Allergen, BitSet>::default();

        for (ingredients, labeled_allergens) in recipes.iter() {
            let possible_allergens = ingredients.iter().copied().collect::<BitSet>();

            for &allergen in labeled_allergens.iter() {
                allergens.entry(allergen).and_modify(|x| {
                    x.intersect_with(&possible_allergens);
                }).or_insert(possible_allergens.clone());
            }
        }

        allergens
    }

    pub fn part1((recipes, _): &(Vec<Recipe>, FnvHashMap<IngredientName, Ingredient>)) -> usize {
        let potential_allergens = potential_allergens(recipes).values().flatten().collect::<BitSet>();

        recipes.iter().map(|(ingredients, _)| ingredients).map(|ingredients| {
            ingredients.iter().filter(|&ingredient| !potential_allergens.contains(*ingredient)).count()
        }).sum()
    }

    pub fn part2((recipes, ingredient_names): &(Vec<Recipe>, FnvHashMap<IngredientName, Ingredient>)) -> String {
        let mut allergens = potential_allergens(recipes);
        let mut bad_ingredients = Vec::new();

        while let Some((allergen, candidates)) = allergens.iter().find(|(_, candidates)| candidates.len() == 1) {
            let bad_ingredient = candidates.iter().next().expect("Length mismatch");

            bad_ingredients.push((bad_ingredient, *allergen));

            for (_, candidates) in allergens.iter_mut() {
                candidates.remove(bad_ingredient);
            };
        }

        // Convert from ingredient ids to names.
        let ingredient_names = ingredient_names.iter().map(|(&k,&v)| (v, k)).collect::<FnvHashMap<Ingredient, IngredientName>>();

        bad_ingredients.iter().sorted_by_key(|&(_, allergen)| unsafe {
            // SAFETY: Allergens are always pointers to strings that are live from `recipes`, and
            // are never modified.
            let allergen_name: &str = &**allergen;
            allergen_name
        }).map(|(ingredient, _)| ingredient_names.get(&ingredient).expect("No ingredient for id")).join(",")
    }
}

pub mod day22 {
    use super::*;
    use std::collections::VecDeque;
    use std::hash::{Hash, Hasher};

    type Deck = VecDeque<u8>;

    pub fn generator(input: &str) -> (Deck, Deck) {
        let mut players = input.split("\n\n").map(|player| {
            player.lines().skip(1).map(|card| card.parse().expect("Invalid card")).collect()
        });

        (players.next().expect("No player 1"), players.next().expect("No player 2"))
    }

    pub fn part1((player1, player2): &(Deck, Deck)) -> usize {
        let (mut deck1, mut deck2) = (player1.clone(), player2.clone());

        while !deck1.is_empty() && !deck2.is_empty() {
            let (card1, card2) = (deck1.pop_front().expect("Just checked"), deck2.pop_front().expect("Just checked"));

            if card1 > card2 {
                deck1.push_back(card1);
                deck1.push_back(card2);
            } else {
                deck2.push_back(card2);
                deck2.push_back(card1);
            };
        }

        let winner = if deck1.len() > 0 {
            deck1
        } else {
            deck2
        };

        winner.into_iter().rev().enumerate().map(|(i, card)| (i+1) * card as usize).sum()
    }

    pub fn part2((player1, player2): &(Deck, Deck)) -> usize {
        let (mut deck1, mut deck2) = (player1.clone(), player2.clone());


        // Returns true if player 1 wins
        fn recursive_game(deck1: &mut Deck, deck2: &mut Deck) -> bool {
            let mut seen = FnvHashSet::<u64>::default();

            while !deck1.is_empty() && !deck2.is_empty() {
                #[inline]
                fn hash(v: &Deck) -> u64 {
                    let mut hasher = fnv::FnvHasher::default();
                    v.hash(&mut hasher);
                    hasher.finish()
                }

                if !seen.insert(hash(deck1)) || !seen.insert(hash(deck2)) {
                    // The problem says "if there was a previous round in this game that had
                    // exactly the same cards in the same order in the same players' decks", then
                    // it's instantly a win.  However, just checking if either has been seen seems
                    // to be sufficient...
                    // Additionally, we represent the deck by its hash, since collisions are
                    // unlikely at this size.
                    return true;
                }

                let (card1, card2) = (deck1.pop_front().expect("Just checked"), deck2.pop_front().expect("Just checked"));

                let p1_wins = match (deck1.len() >= card1.into(), deck2.len() >= card2.into()) {
                    (true, true) => recursive_game(
                        &mut deck1.iter().take(card1.into()).copied().collect(),
                        &mut deck2.iter().take(card2.into()).copied().collect()
                    ),
                    _ => card1 > card2
                };

                if p1_wins {
                    deck1.push_back(card1);
                    deck1.push_back(card2);
                } else {
                    deck2.push_back(card2);
                    deck2.push_back(card1);
                };
            }

            // If a deck has cards left, it must've won.
            deck1.len() > 0
        }

        let winner = if recursive_game(&mut deck1, &mut deck2) {
            deck1
        } else {
            deck2
        };

        winner.into_iter().rev().enumerate().map(|(i, card)| (i+1) * card as usize).sum()
    }
}

pub mod day23 {
    use super::*;

    type InputCup = u8;

    pub fn generator(input: &str) -> Vec<InputCup> {
        input.trim().bytes().map(|x| x - b'0').collect()
    }

    pub fn part1(cups: &[InputCup]) -> String {
        const TOTAL_CUPS: usize = 9;
        const MOVES: usize = 100;

        type Cup = u8;
        assert!(TOTAL_CUPS < Cup::MAX as usize, "Cup size too small for TOTAL_CUPS");

        // NOTE: We want to use a linked list and reference nodes here, but that doesn't work
        // because we can't reference nodes properly while mutating the list.
        // Instead, we build an implicit linked list out of indices.
        let mut current = cups[0] as Cup;

        // Represents a mapping of (cup -> next cup)
        let mut indices = Vec::<Cup>::default();
        indices.resize(TOTAL_CUPS as usize + 1, 0);

        let cups = cups.iter().map(|&x| x as Cup).chain((cups.len()+1..=TOTAL_CUPS).map(|x| x as Cup));
        for (cup, next) in cups.clone().zip(cups.cycle().skip(1)) {
            indices[cup as usize] = next;
        }

        for _ in 0..MOVES {
            let second = indices[current as usize];
            let third = indices[second as usize];
            let fourth = indices[third as usize];

            let mut destination = current - 1;

            loop {
                if destination == 0 {
                    destination = TOTAL_CUPS as Cup;
                }

                if second != destination && third != destination && fourth != destination {
                    break;
                }

                destination -= 1;
            };

            // Add the picked elements to the list.
            let tail = std::mem::replace(&mut indices[destination as usize], second);
            let next = std::mem::replace(&mut indices[fourth as usize], tail);

            // Rotate one.
            indices[current as usize] = next;
            current = next;
        }

        (0..TOTAL_CUPS).fold((String::new(), 1), |(mut string, head), _| {
            string.push((indices[head] + b'0') as char);
            (string, indices[head] as usize)
        }).0
    }

    pub fn part2(cups: &[InputCup]) -> usize {
        const TOTAL_CUPS: usize = 1000000;
        const MOVES: usize = 10000000;

        type Cup = u32;
        assert!(TOTAL_CUPS < Cup::MAX as usize, "Cup size too small for TOTAL_CUPS");

        // NOTE: We want to use a linked list and reference nodes here, but that doesn't work
        // because we can't reference nodes properly while mutating the list.
        // Instead, we build an implicit linked list out of indices.
        let mut current = cups[0] as Cup;

        // Represents a mapping of (cup -> next cup)
        let mut indices = Vec::<Cup>::default();
        indices.resize(TOTAL_CUPS as usize + 1, 0);

        let cups = cups.iter().map(|&x| x as Cup).chain((cups.len()+1..=TOTAL_CUPS).map(|x| x as Cup));
        for (cup, next) in cups.clone().zip(cups.cycle().skip(1)) {
            indices[cup as usize] = next;
        }

        for _ in 0..MOVES {
            let second = indices[current as usize];
            let third = indices[second as usize];
            let fourth = indices[third as usize];

            let mut destination = current - 1;

            loop {
                if destination == 0 {
                    destination = indices.len() as Cup - 1;
                }

                if second != destination && third != destination && fourth != destination {
                    break;
                }

                destination -= 1;
            };

            // Add the picked elements to the list.
            let tail = std::mem::replace(&mut indices[destination as usize], second);
            let next = std::mem::replace(&mut indices[fourth as usize], tail);

            // Rotate one.
            indices[current as usize] = next;
            current = next;
        }

        indices[1] as usize * indices[indices[1] as usize] as usize
    }
}

pub mod day24 {
    use super::*;

    pub enum Dir {
        W,
        NW,
        SW,
        E,
        SE,
        NE,
    }

    type Coord = (i16, i16);

    pub fn generator(input: &str) -> Vec<Coord> {
        input.lines().map(|line| {
            let mut coord = (0,0);

            let mut i = 0;
            while i < line.len() {
                coord = match (line.as_bytes()[i], line.as_bytes().get(i+1)) {
                    (b'e', _) => (coord.0 + 1, coord.1),
                    (b'w', _) => (coord.0 - 1, coord.1),
                    (b's', Some(b'e')) => {
                        i += 1;
                        (coord.0 + 1, coord.1 - 1)
                    },
                    (b's', Some(b'w')) => {
                        i += 1;
                        (coord.0, coord.1 - 1)
                    },
                    (b'n', Some(b'w')) => {
                        i += 1;
                        (coord.0 - 1, coord.1 + 1)
                    },
                    (b'n', Some(b'e')) => {
                        i += 1;
                        (coord.0, coord.1 + 1)
                    },
                    _ => panic!("Invalid input")
                };

                i += 1;
            }

            coord
        }).collect()
    }

    pub fn part1(tiles: &[Coord]) -> usize {
        let mut counter = FnvHashMap::default();

        for tile in tiles.iter() {
            counter.entry(tile).and_modify(|count| *count += 1).or_insert(1);
        }

        counter.iter().filter(|(_, &flipped)| flipped % 2 == 1).count()
    }

    pub fn part2(tiles: &[Coord]) -> usize {
        const ROUNDS: usize = 100;

        let mut counter = FnvHashMap::default();
        for tile in tiles.iter() {
            counter.entry(tile).and_modify(|count| *count += 1).or_insert(1);
        }
        let mut active_cells = counter.iter().filter(|(_, &flipped)| flipped % 2 == 1).map(|(&&c, _)| c).collect::<FnvHashSet<Coord>>();

        let offsets = [
            (1, 0),
            (-1, 0),
            (1, -1),
            (0, -1),
            (-1, 1),
            (0, 1),
        ];

        for _ in 0..ROUNDS {
            let mut neighbors = FnvHashMap::<Coord, u8>::default();

            // Hit the neighbor of each active cell.
            for (x,y) in active_cells.iter() {
                for off in offsets.iter() {
                    let neighbor = (x + off.0, y + off.1);

                    neighbors.entry(neighbor).and_modify(|x| *x += 1).or_insert(1);
                }
            }

            // Cells must have been hit to be active, so for each cell that was hit check if
            // they should be activated.
            let next_active = neighbors.iter().filter_map(|(c, hits)| match hits {
                2 => Some(c),
                1 if active_cells.contains(c) => Some(c),
                _ => None,
            }).copied().collect::<FnvHashSet<Coord>>();

            active_cells = next_active;
        }

        active_cells.iter().count()
    }
}

pub mod day25 {
    use super::*;

    type PublicKey = usize;

    pub fn generator(input: &str) -> (PublicKey, PublicKey) {
        let keys = input.lines().map(|line| line.parse().expect("Invalid key")).collect::<ArrayVec<PublicKey, 2>>();

        (keys[0], keys[1])
    }

    pub fn part1(&(card_public, door_public): &(PublicKey, PublicKey)) -> usize {
        const MODULUS: usize = 20201227;

        // Bruteforce the exponent
        let card_loops = {
            let mut loops = 0;
            let mut value = 1;

            while value != card_public {
                value = (value * 7) % MODULUS;
                loops += 1;
            };

            loops
        };

        // Modular exponentiation shortcut
        let mut result = 1;
        let mut base = door_public % MODULUS;
        let mut exp = card_loops;

        while exp > 0 {
            if exp % 2 == 1 {
                result = (result * base) % MODULUS;
            }

            exp >>= 1;
            base = (base * base) % MODULUS;
        }
        result
    }

    pub fn part2(_: &(PublicKey, PublicKey)) -> &'static str {
        "merry christmas"
    }
}
